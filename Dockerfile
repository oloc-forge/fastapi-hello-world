FROM python:3.10-slim
LABEL maintainer="Olivier Locard"

ENV PYTHONPATH .
ARG DIR='srv'
ARG APP='hello_world'

RUN pip install --upgrade pip
WORKDIR /$DIR
COPY ./requirements.txt /$DIR/requirements.txt
COPY ./$APP /$DIR/$APP
RUN pip install --no-cache-dir --upgrade --requirement requirements.txt

CMD ["uvicorn", "hello_world.main:app", "--host", "0.0.0.0", "--port", "8000"]