#!/usr/bin/env python
# -*-coding:UTF-8 -*
#
# Olivier Locard
# pylint: disable=missing-module-docstring

from typing import Union
from fastapi import FastAPI
import uvicorn

app = FastAPI()


@app.get("/")
def read_root():  # pylint: disable=missing-function-docstring
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, quantity: Union[str, None] = None):  # pylint: disable=missing-function-docstring
    return {"item_id": item_id, "q": quantity}


if __name__ == "__main__":
    config = uvicorn.Config(app=app, port=8000, log_level="info")
    server = uvicorn.Server(config)
    server.run()
