# Hello World

This project is the basics to start a FastAPI python project building a docker image.

## Usage

    image='registry.gitlab.com/oloc-forge/fastapi-hello-world'
    docker run -d --name mycontainer -p 8000:8000 $image

* Open your browser at http://127.0.0.1:8000
* go to http://127.0.0.1:8000/docs
* and go to  http://127.0.0.1:8000/items/5?q=somequery

## License

ISC

## Author information

This code was created in 2022 by Olivier Locard.

