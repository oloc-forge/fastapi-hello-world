#!/usr/bin/env python
# -*-coding:UTF-8 -*
#
# Olivier Locard

import pytest
from fastapi.testclient import TestClient

from hello_world.main import app


@pytest.fixture
def client():
    return TestClient(app)


def test_read_root(client):
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"Hello": "World"}


def test_read_item(client):
    response = client.get("/items/5?quantity=somequery")
    assert response.status_code == 200
    assert response.json() == {'item_id': 5, 'q': 'somequery'}
